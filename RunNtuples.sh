#!/bin/sh                                                                       
file=$1
params=$2
config=$3
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export DQ2_LOCAL_SITE_ID=DESY-HH_SCRATCHDISK
alias setupATLAS='source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh'
setupATLAS
export ALRB_rootVersion=6.18.04-x86_64-centos7-gcc8-opt
lsetup root
./GenCalibNtuples $file $params $config